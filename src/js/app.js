App = {
  web3Provider: null,
  contracts: {},

  init: function() {
    return App.initWeb3();
  },

  initWeb3: function() {
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
    } else {
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
    }
    web3 = new Web3(App.web3Provider);
    return App.initContract();
  },

  initContract: function() {
    // Listen for account change and reload avatar
    web3.currentProvider.publicConfigStore.on('update', function() {
      App.loadAvatar();
    });

    $.getJSON('./js/bank-abi.json', function(data) {
      var BankArtifact = data;
      App.contracts.Bank = TruffleContract(BankArtifact);
      App.contracts.Bank.setProvider(App.web3Provider);
    })
    App.loadAvatar();
    return App.loadInformation();
  },

  loadAvatar: function() {
    web3.eth.getAccounts(function(error, accounts) {
      if (error) { console.log(error) }
      account = accounts[0];
      imgUrl = "https://robohash.org/" + account + "?set=set4";
      $("#avatar-img").attr("src", imgUrl);
      $("#avatar-img").load(function(){
        $("#avatar-box").show();
      });
    });
  },

  loadInformation: function() {
    var account, bankInstance, saving, total;

    // Watch account and contract balances every second
    setInterval(function () {
      // Load account and balance
      web3.eth.getAccounts(function(error, accounts) {
        if (error) { console.log(error) }
        account = accounts[0];
        $("#account").text(account);
        web3.eth.getBalance(account, function(err, balance){
          var bal = web3.fromWei(web3.toDecimal(balance), 'ether');
          var formatted = parseFloat(bal, 10).toFixed(2);
          $("#balance").text(formatted);
        })
      });

      // Load information from contract
      web3.eth.getAccounts(function(error, accounts) {
        if (error) { console.log(error) }
        var account = accounts[0];
        App.contracts.Bank.deployed()
          .then(function(instance) {
            bankInstance = instance;
            return bankInstance.getBalance.call();
          })
          .then(function(result) {
            total = web3.fromWei(web3.toDecimal(result), 'ether');
            $("#total").text(total);
            return bankInstance.balances.call(account);
          })
          .then(function(result) {
            saving = web3.fromWei(web3.toDecimal(result), 'ether');
            $("#saving").text(saving);
          })
          .catch(function(err) {
            console.log(err.message);
          });
      });
    }, 1000);

    return App.bindEvents();
  },

  bindEvents: function() {
    $(document).on('click', '#topup', App.handleTopup);
    $(document).on('click', '#withdraw', App.handleWithdraw);
  },

  handleTopup: function() {
    var val = $("#ether_to_topup").val();
    var bankInstance;

    if (val == "") {
      return iziToast.show({
        title: 'Error',
        message: 'Please enter ETH amount',
        color: 'red',
        position: 'bottomRight'
      });
    }

    web3.eth.getAccounts(function(error, accounts) {
      if (error) { console.log(error) }
      var account = accounts[0];
      App.contracts.Bank.deployed()
        .then(function(instance) {
          bankInstance = instance;
          return web3.eth.sendTransaction({
            from: account,
            to: bankInstance.address,
            value: web3.toWei(val)
          }, function(err, result) {
            if (err == null) {
              iziToast.show({
                title: 'Submitted',
                message: result,
                color: 'yellow',
                position: 'bottomRight'
              });
              $("#ether_to_topup").val("");
              return App.watchTransaction(result);
            }
          });
        });
    });
  },

  handleWithdraw: function() {
    var val = $("#ether_to_withdraw").val();
    var bankInstance;

    if (val == "") {
      return iziToast.show({
        title: 'Error',
        message: 'Please enter ETH amount',
        color: 'red',
        position: 'bottomRight'
      });
    }

    web3.eth.getAccounts(function(error, accounts) {
      if (error) { console.log(error) }
      var account = accounts[0];
      App.contracts.Bank.deployed()
        .then(function(instance) {
          bankInstance = instance;
          return bankInstance.withdraw.sendTransaction(web3.toWei(val));
        })
        .then(function(result) {
          iziToast.show({
            title: 'Submitted',
            message: result,
            color: 'yellow',
            position: 'bottomRight'
          });
          $("#ether_to_withdraw").val("");
          return App.watchTransaction(result);
        })
    });
  },

  watchTransaction: function(txHash) {
    var watcher = setInterval(function () {
      // console.log("Checking transaction +", txHash);
      web3.eth.getTransactionReceipt(txHash, function(err, result){
        if (result && result.status == "0x1") {
          clearInterval(watcher);
          iziToast.show({
            title: 'Success',
            message: 'Transaction mined. ETH balance will be updated shortly.',
            color: 'green',
            position: 'bottomRight'
          });
        }
      })
    }, 500);
  },

};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
