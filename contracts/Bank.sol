pragma solidity ^0.4.22;

contract Bank {
    mapping(address => uint256) public balances;

    event Topup(address from, uint256 amount);
    event Withdrawal(address to, uint256 amount);

    function() public payable {
        balances[msg.sender] += msg.value;
        emit Topup(msg.sender, msg.value);
    }

    function withdraw(uint256 amount) public {
        require(balances[msg.sender] > 0, "Sender does not has ETH here");
        require(amount <= balances[msg.sender], "Amount exceeds balance");
        balances[msg.sender] -= amount;
        msg.sender.transfer(amount);
        emit Withdrawal(msg.sender, amount);
    }

    function getBalance() public view returns (uint256) {
        return this.balance;
    }
}
